import {http,_baseUrl} from './index'

// function logIn(data) {
//     return http('login', data, 'post')
// }

// function getMenus() {
//     return http('menus')
// }


// export default {
//     logIn,
//     getMenus
// }


export const logIn = function(data) {
    return http('login', data, 'post')
}

export const getMenus = function() {
    return http('menus')
}

export const getUsers = function(params) {
    return http('users',{} ,'get' ,params)
}

export const addUsers = function(data) {
    return http('users', data, 'POST')
}

export const changeUserStatus = function(data) {
    return http(`users/${data.id}/state/${data.mg_state}`, {}, 'PUT')
}

export const editUsers = function(data) {
    return http(`users/${data.id}`, {
        email: data.email,
        mobile: data.mobile
    }, 'PUT')
}

export const deleteUsers = function(data) {
    return http(`users/${data.id}+val`, {}, 'DELETE')
}

export const getUserInfoById = function(data) {
    return http(`users/${data.id}`, {}, 'GET')
}

export const getRolesList = function() {
    return http('roles', {}, 'GET')
}

export const editRole = function(data) {
    return http(`users/${data.id}/role`, {
        rid: data.rid
    }, 'PUT')
}

export const deleteRights = function(roleId, rightsId) {
    return http(`roles/${roleId}/rights/${rightsId}`,{}, 'DELETE')
}

//添加角色
export const addRoles = function(data) {
    return http(`roles`, data, "POST")
}

// 获取权限

export const getRights=function(val){
    return http(`rights/${!val?'tree':'list'}`)
}

// 修改权限
export const editRights=function(roleId,rids){
    return http(`roles/${roleId}/rights`,{
        rids
    },'POST')
}

export const deleteRoles = function(data) {
    return http(`roles/${data.id}+val`, {}, 'DELETE')
}


export const deleteGoods = function(data) {
    return http(`goods/${data.id}+val`, {}, 'DELETE')
}
// 获取商品列表
export const getGoodsLists=function(data){
    return http('goods',{},'GET',data)
}


//获取商品分类列表接口给
export const getCategoriesLists = function() {
    return http('categories?type=3')
}

//获取动态参数
export const getManyAttr = function(id) {
    return http(`categories/${id}/attributes?sel=many`, {}, 'GET')
}

//获取静态参数
export const getOnlyAttr = function(id) {
    return http(`categories/${id}/attributes?sel=only`, {}, 'GET')
}

//上传接口
export const uploadImg = _baseUrl + 'upload'


//发布商品
export const publicGoods = function(data) {
    return http('goods', data, 'POST')
}



