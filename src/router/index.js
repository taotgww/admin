import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/Login'
import Home from '@/views/Home'
import store from '@/store'
import Users from '@/views/users/Users'
import Roles from '@/views/rights/Roles'
import Rights from '@/views/rights/Rights'
import Goods from '@/views/goods/Goods'
import AddGoods from '@/views/goods/AddGoods'
import Params from '@/views/goods/Params'
import Categories from '@/views/goods/Categories'
import Orders from '@/views/orders/Orders'
import Reports from '@/views/reports/Reports'


const originalPush=Router.prototype.push
Router.prototype.push=function push(location){
  return originalPush.call(this,location).catch(err=>err)
}

Vue.use(Router)

const routes=new Router({
  routes: [
    {
      path:'/',
      redirect:'/login'
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta:{
        requiresAuth:true
      }
    },
    {
      path:'/home',
      name:'home',
      component:Home,
      children:[
        {
          path:'users',
          name:'users',
          component:Users
        },
        {
          path:'roles',
          name:'roles',
          component:Roles
        },
        {
          path:'rights',
          name:'rights',
          component:Rights
        },
        {
          path:'goods',
          name:'goods',
          component:Goods
        },
        {
          path:'addgoods',
          name:'addgoods',
          component:AddGoods
        },
        {
          path:'params',
          name:'params',
          component:Params
        },
        {
          path:'categories',
          name:'categories',
          component:Categories
        },
        {
          path:'orders',
          name:'orders',
          component:Orders
        },
        {
          path:'reports',
          name:'reports',
          component:Reports
        }
      ]
    }
  ],
  linkActiveClass:'actived-class'
})

routes.beforeEach((to,from,next)=>{
  if(!to.meta.requiresAuth){
    if(store.state.token){
      next()
    }else{
      next({
        path:'/login'
      })
    }
  }else{
    next()
  }
})

export default routes